import { configureStore } from "@reduxjs/toolkit";
import sendReducer from './send'
import themeReducer from './theme'
const store = configureStore({
    reducer: {
        send: sendReducer,
        theme: themeReducer,
    }
})



export default store;