import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    darkmode: false,
}

const themeSlice = createSlice({
    name: 'theme',
    initialState,
    reducers: {
        toggle(state){
            state.darkmode = !state.darkmode
        }
    }
})
export const themeActions = themeSlice.actions
export default themeSlice.reducer