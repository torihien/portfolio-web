import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isDone: false,
}

const sendSlice = createSlice({
    name: 'send',
    initialState,
    reducers: {
        sendDone(state,action){
            state.isDone = true
        },
    }
})

export const sendActions = sendSlice.actions
// export const sendActions = sendSlice.actions
export default sendSlice.reducer