import React from "react";
import styles from './Card.module.scss'
import { motion } from "framer-motion";
const Card = ({emoji,alt,heading,detail,className,i,w}) => {
    const transition = {duration: 1, type:'spring'}
    
    return (
        <motion.div
        initial = {i}
        whileInView={w}
        transition = {transition}
        className={`${styles.card} ${className}`}>
            <img src={emoji} alt={alt} />
            <span>{heading}</span>
            <span>{detail}</span>
            <button className={styles.bt}>LEARN MORE</button>
        </motion.div>
    )
}

export default Card;