import React from "react"
import styles from "./Navbar.module.scss"
import Toggle from "./Toggle/Toggle"
import { Link } from "react-scroll"

const Navbar = () =>{
    
    return(
        <div className={styles.nav}>
            <div className={styles['wrapper']} id='scroll-wrapper'>
                <div className={styles['left']}>
                    <div className = {styles['name']}>Tori Hien</div>
                    <span><Toggle /></span>
                </div>
                <div className={styles['right']}>
                    <div className={styles['list']}>
                        <ul>
                        <Link spy={true} to='scroll-wrapper' smooth={true} activeClass='activeClass'>
                            <li>Home</li>
                        </Link>
                        <Link spy={true} to='scroll-services' smooth={true} activeClass='activeClass'>
                            <li>Services</li>
                        </Link>
                        <Link spy={true} to='scroll-experience' smooth={true} activeClass='activeClass'>
                            <li>Experience</li>
                        </Link>
                        <Link spy={true} to='scroll-portfolio' smooth={true} activeClass='activeClass'>
                            <li>Portfolio</li>
                        </Link>
                        <Link spy={true} to='scroll-testimonial' smooth={true} activeClass='activeClass'>
                            <li>Testimonials</li>
                        </Link>
                        </ul>
                    </div>
                    <button className={styles.bt}>
                        Contact us
                    </button> 
                </div>
            </div>
        </div>
    )
}
export default Navbar