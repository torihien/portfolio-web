import React from "react";
import styles from './Toggle.module.scss'
import Sun from '@iconscout/react-unicons/icons/uil-sun'
import Moon from '@iconscout/react-unicons/icons/uil-moon'
import { useSelector, useDispatch } from "react-redux";
import { themeActions } from "../../../store/theme";

const Toggle = () => {
    const dispatch = useDispatch();
    const darkmode = useSelector(state => state.theme.darkmode)
    const toggle = () => (dispatch(themeActions.toggle()))

    return (
        <div className={styles.toggle} onClick={toggle}>
            <Sun />
            <Moon />
            <div className={darkmode ? `${styles.bt} ${styles['dark-bt']}` : styles.bt } />
            {/* <div className={styles.bt} style = {darkmode ? {left:'2px'} :{right: '2px'}} /> */}
        </div>
    )
}

export default Toggle