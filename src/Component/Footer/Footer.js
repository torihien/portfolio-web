import React from "react";
import styles from './Footer.module.scss'
import Wave from '../../img/wave.png'
import Insta from '@iconscout/react-unicons/icons/uil-instagram'
import Facebook from '@iconscout/react-unicons/icons/uil-facebook'
import Github from '@iconscout/react-unicons/icons/uil-github'

const Footer = () => {
    return (
        <div className={styles.footer}>
            <img src={Wave} alt='Wave'/>
            <div className = {styles.content}>
                <span>torihien@gmail.com</span>
                <div className={styles.icons}>
                    <Insta color='white' size='3rem'/>
                    <Facebook color='white' size='3rem'/>
                    <Github color='white' size='3rem'/>
                </div>
            </div>
        </div>
    )
}

export default Footer