import React from "react";
import styles from './Intro.module.scss'
import Github from '../../img/github.png'
import LinkedIn from '../../img/linkedin.png'
import Instagram from '../../img/instagram.png'
import Vector1 from '../../img/Vector1.png'
import Vector2 from '../../img/Vector2.png'
import Boy from '../../img/boy.png'
import Thumbup from '../../img/thumbup.png'
import Crown from '../../img/crown.png'
import Glassesimoji from '../../img/glassesimoji.png'
import FloatingDiv from "./FloatingDiv/FloatingDiv";
import { useSelector } from "react-redux";
import { motion } from "framer-motion";
const Intro = () => {
    const darkmode = useSelector(state => state.theme.darkmode)
    const transition = {duration : 2, type: 'spring'}


    return(
        <div className = {styles.intro}>
            <div className = {styles.left}>
                <div className = {darkmode ? `${styles.name} ${styles['dark-name']}`:styles.name}>
                    <span>Hy! I am</span>
                    <span>Tori Hien</span>
                    <span>Frontend Developer with high level of experience in web designing and development, producting the Quality work.</span>
                </div>
                <button className = {styles.bt}>Hire me</button>
                <div className={styles.icons}>
                    <img src={Github} alt='Github'/>
                    <img src={LinkedIn} alt='LinkedIm'/>
                    <img src={Instagram} alt='Instagram'/>
                </div>
            </div>
            <div className = {styles.right}>
                <img src={Vector1} alt='Vector2'/>
                <img src={Vector2} alt='Vector2'/>
                <img src={Boy} alt='Boy'/>
                <motion.img 
                initial={{left:'-36%'}}
                whileInView = {{left: '-24%'}}
                transition= {transition}
                src={Glassesimoji} alt = 'Glassesimoji'/>
                <FloatingDiv
                i={{top:'-4%',left:'74%'}}
                w={{left:'68%'}}
                image={Crown} 
                alt = 'Crown '
                txt1='Web' 
                txt2 = 'Developer' 
                className={styles.zone1}/>
                <FloatingDiv 
                i={{top:'17.5rem',left:'18rem'}}
                w={{left:'1.4rem'}}
                image={Thumbup} 
                alt = 'Thumbup ' 
                txt1='Best Design' 
                txt2 = 'Award' 
                className={styles.zone2}/>
                <div className={styles.blur1}></div>
                <div className={styles.blur2}></div>
            </div>
        </div>
    )
}

export default Intro