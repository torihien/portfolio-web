import React from "react"
import styles from './Work.module.scss'
import Upwork from '../../../img/Upwork.png'
import Fiverr from '../../../img/fiverr.png'
import Amazone from '../../../img/amazon.png'
import Shopify from '../../../img/Shopify.png'
import Facebook from '../../../img/Facebook.png'
import { useSelector } from "react-redux"
import { motion } from "framer-motion"
const Works = () => {
    const darkmode = useSelector(state => state.theme.darkmode)
    const transition = {duration: 3.5, type: 'spring'}
    
    return (
        <div className = {darkmode ?`${styles.works} ${styles['dark-works']}` :styles.works} >
            <div className = {styles.awesome}>
                <span>Works for All these</span>
                <span>Brands & Clients</span>
                <span>
                    This is my Work. 
                    <br/>
                    You can check its by clicking on this button. 
                    <br/>
                    Lorem is resume what are you doing oh hello my body 
                    <br/>
                    Lorem is resume what are you doing oh hello my body
                </span>
                <button className = {styles.bt}>Download CV</button>
                <div className = {styles.blur1}></div>
            </div>
            <div className={styles.right}>
                <motion.div 
                initial = {{rotate: 45}}
                whileInView={{rotate: 0}}
                viewport={{margin:'-40px'}}
                transition={transition}
                className={styles.mainCircle}>
                    <div className = {styles.secCircle}>
                        <img src={Upwork} alt="Upwork"/>
                    </div>
                    <div className = {styles.secCircle}>
                        <img src={Fiverr} alt="Upwork"/>
                    </div>
                    <div className = {styles.secCircle}>
                        <img src={Amazone} alt="Upwork"/>
                    </div>
                    <div className = {styles.secCircle}>
                        <img src={Shopify} alt="Upwork"/>
                    </div>
                    <div className = {styles.secCircle}>
                        <img src={Facebook} alt="Upwork"/>
                    </div>
                </motion.div>
                <div className = {`${styles.backCircle} ${styles.blueCircle}`}></div>
                <div className = {`${styles.backCircle} ${styles.yellowCircle}`}></div>
            </div>
        </div>
    )
}

export default Works