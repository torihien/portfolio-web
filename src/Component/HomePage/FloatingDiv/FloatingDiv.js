import React from "react";
import styles from './FloatingDiv.module.scss'
import { motion } from "framer-motion";

const FloatingDiv = ({image,alt,txt1,txt2, className,i,w}) => {
    const transition = {duration: 2, type: 'spring'}
    
    return(
        <>
        <motion.div 
        initial={i}
        whileInView = {w}
        transition= {transition}
        className ={`${styles.floatingdiv} ${className}`} >
            <img src = {image} alt={alt} />
            <span>
                {txt1}
                <br/>
                {txt2}
            </span> 
        </motion.div>
        </>
    )
}
export default FloatingDiv