import React from "react";
import styles from './Testimonial.module.scss'
import { Swiper, SwiperSlide } from "swiper/react";
import 'swiper/scss'
import profilePic1 from "../../../img/profile1.jpg";
import profilePic2 from "../../../img/profile2.jpg";
import profilePic3 from "../../../img/profile3.jpg";
import profilePic4 from "../../../img/profile4.jpg";
import { Pagination } from "swiper"
import 'swiper/scss/pagination'


const Testimonial = () => {
    const clients = [
        {
            img: profilePic1,
            review:
              "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ex officiis molestiae quod tempora laudantium, cumque error a nisi placeat quae exercitationem, maiores reiciendis! Eaque dicta minima, iure maiores dolorum sed.",
          },
          {
            img: profilePic2,
            review:
              "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ex officiis molestiae quod tempora laudantium, cumque error a nisi placeat quae exercitationem, maiores reiciendis! Eaque dicta minima, iure maiores dolorum sed.",
          },
          {
            img: profilePic3,
            review:
              "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ex officiis molestiae quod tempora laudantium, cumque error a nisi placeat quae exercitationem, maiores reiciendis! Eaque dicta minima, iure maiores dolorum sed.",
          },
          {
            img: profilePic4,
            review:
              "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ex officiis molestiae quod tempora laudantium, cumque error a nisi placeat quae exercitationem, maiores reiciendis! Eaque dicta minima, iure maiores dolorum sed.",
          },
    ]

    return (
        <div className={styles.wrapper} id = 'scroll-testimonial'>
            <div className={styles.heading}>
                <span>Client always get</span>
                <span> Exceptional Work</span>
                <span> from me...</span>
                <div className={styles.blur1}></div>
                <div className={styles.blur2}></div>
            </div>
            
            <Swiper 
            modules = {[Pagination]}
            slidesPerView={1}
            pagination = {{clickable: true}}
            className={styles.swiper}
            >
                {clients.map((client,index)=> {
                    return (
                        <SwiperSlide key = {index} className={styles['swiper-slide']}>
                            <div className={styles.testimonial}>
                                <img src = {client.img} alt='' />
                                <span>{client.review}</span>
                            </div>
                        </SwiperSlide>
                    )
                })}
            </Swiper> 
        </div>
        
    )
}

export default Testimonial