import React from "react";
import styles from './Contact.module.scss'
import emailjs from '@emailjs/browser'
import { useRef } from "react";
import { useSelector, useDispatch} from "react-redux";
import { sendActions } from "../../../store/send";

const Contact = () => {
    const form = useRef()
    const dispatch = useDispatch()
    const isDone = useSelector(state => state.send.isDone)
    const sendDone = (a) => {dispatch(sendActions.sendDone(a))}
    

    const sendEmail = (e) => {
        e.preventDefault()
        emailjs.sendForm('service_p6q2vym', 'template_5hd25ha', form.current, '3xHAdTem6bqFUvpYa')
          .then((result) => {
              console.log(result.text)
              sendDone(true);
          }, (error) => {
              console.log(error.text)
          });
      };


    

    return (
        <div className={styles['contact-form']}>
            <div className = {styles.left}>
                <div className = {styles.awesome}>
                    <span>Get in touch</span>
                    <span>Contact me</span>
                </div>    
                <div
                    className={styles.blur1}
                ></div>
            </div>

            <div className = {styles.right}>
                <form ref = {form} onSubmit={sendEmail}>
                    <input type='text' name ='user_name' className={styles.user} placeholder="Name" />
                    <input type='email' name ='user_email' className={styles.user} placeholder="Email" />
                    <textarea name='message' className={styles.user} placeholder="Message" />
                    <input type='submit' value='Send' className={styles.bt} />
                    <div className={styles.blur2}></div>
                    <span>{isDone && 'thanks for contact me'}</span>
                </form>
            </div>
        </div>
    )
}

export default Contact