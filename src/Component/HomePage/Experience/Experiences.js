import React from "react";
import styles from './Experiences.module.scss'
import { useSelector } from "react-redux";
const Experience = () => {
    const darkmode = useSelector(state => state.theme.darkmode)
    return (
        <div className={darkmode ?`${styles.experience} ${styles['dark-experience']}` : styles.experience} id='scroll-experience'>
            <div className={styles.achivement}>
                <div className={styles.circle}>8+</div>
                <span>years</span>
                <span>Experience</span>
            </div>
            <div className={styles.achivement}>
                <div className={styles.circle}>20+</div>
                <span>completed</span>
                <span>Projects</span>
            </div>
            <div className={styles.achivement}>
                <div className={styles.circle}>5+</div>
                <span>companies</span>
                <span>work</span>
            </div>
        </div>
    )
}

export default Experience