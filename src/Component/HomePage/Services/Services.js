import React from "react"
import styles from './Services.module.scss'
import Card from '../../Card/Card'
import HeartEmoji from '../../../img/heartemoji.png'
import Glasses from '../../../img/glasses.png'
import Humble from '../../../img/humble.png'
import Resume from './MyResume.pdf'
import { useSelector } from "react-redux"

const Services = () => {
    const darkmode = useSelector(state => state.theme.darkmode)

    
    return (
        <div className = {styles.services} id='scroll-services'>
            <div className = {darkmode ? `${styles.awesome} ${styles['dark-awesome']}`:styles.awesome}>
                <span>My awesome</span>
                <span>services</span>
                <span>
                    This is my CV. 
                    <br/>
                    You can download file by clicking on this button. 
                </span>
                <a href={Resume} download>
                    <button className = {styles.bt}>Download CV</button>
                </a>
            </div>
            <div className = {styles.cards}>
                <Card
                i={{left: '75%'}}
                w={{left: '17rem'}}
                emoji = {HeartEmoji}
                heading = {'Design'}
                detail = {'Figma, Sketch, Photoshop, Adobe, AdobeXd'}
                className={styles.service1}
                />
                <Card
                i={{left:'-50%'}}
                w={{left:'-8rem'}} 
                emoji = {Glasses}
                heading = {"Developer"}
                detail = {"Html, Css, Javascript, React"}
                className = {styles.service2}
                />
                <Card 
                i = {{left:'65%'}}
                w = {{left: '12rem'}}
                emoji = {Humble}
                heading = {"UI/UX"}
                detail = {"Design everything from software to hardware, house design"}
                className = {styles.service3}
                />
                <div className={styles.blur1}></div>
                <div className={styles.blur2}></div>
            </div>
        </div>
    )
}

export default Services