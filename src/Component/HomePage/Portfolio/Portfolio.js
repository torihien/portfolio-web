import React from "react";
import styles from './Portfolio.module.scss'
import {Swiper, SwiperSlide } from "swiper/react";
import SideBar from '../../../img/sidebar.png'
import Ecommerce from '../../../img/ecommerce.png'
import MusicApp from '../../../img/musicapp.png'
import HOC from '../../../img/hoc.png'
import 'swiper/scss'
import { useSelector } from "react-redux";
const Portfolio = () => {
    const darkmode = useSelector(state => state.theme.darkmode) 
    const windowWidth = window.innerWidth
    return (
        <div className={darkmode ? `${styles.portfolio} ${styles['dark-portfolio']}` : styles.portfolio} id='scroll-portfolio'>
            <span>Recent Projects</span>
            <span>Portfolio</span>
            <Swiper
            spaceBetween={30}
            slidesPerView={windowWidth > 480 ? 3 : 1} 
            grabCursor = {true}
            onSlideChange={()=>console.log('slide change')}
            className = {styles['portfolio-slider']}
            >
                    <SwiperSlide>
                        <img src={SideBar} alt='' />
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={Ecommerce} alt='' />
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={MusicApp} alt='' />
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={HOC} alt='' />
                    </SwiperSlide>
            </Swiper>
        </div>
    )
}

export default Portfolio;
