import styles from './App.module.scss'
import HomePage from './Pages/HomePage';
import { Route,Routes, Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';


function App() {
  const darkmode = useSelector(state => state.theme.darkmode)
  
  return (
    <div className={darkmode ?  `${styles.app} ${styles['dark-app']}` : styles.app} >
      <Routes>
        <Route path = '/*' element = {<Navigate to='/home' />} />
        <Route path ='/home' element = {<HomePage />} /> 
      </Routes>
    </div>
  );
}

export default App;
