import Navbar from "../Component/Navbar/Navbar";
import Intro from '../Component/HomePage/Intro';
import Services from "../Component/HomePage/Services/Services";
import Experience from "../Component/HomePage/Experience/Experiences";
import Works from "../Component/HomePage/Work/Work";
import Portfolio from "../Component/HomePage/Portfolio/Portfolio";
import Testimonial from "../Component/HomePage/Testimonial/Testimonial";
import Contact from "../Component/HomePage/Contact/Contact";
import Footer from "../Component/Footer/Footer";

function HomePage() {
  return (
    <>
        <Navbar />
        <Intro />
        <Services />
        <Experience />
        <Works />
        <Portfolio />
        <Testimonial />
        <Contact />
        <Footer />
    </>
  );
}

export default HomePage
